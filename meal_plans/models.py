from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField(editable=True, auto_now=False)
    owner = models.ForeignKey(
        USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        related_name="meal_plan",
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")
