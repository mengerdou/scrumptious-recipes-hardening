from django.urls import path

from meal_plans.views import (
    Meal_PlansCreateView,
    Meal_PlansDeleteView,
    Meal_PlansUpdateView,
    Meal_PlansDetailView,
    Meal_PlansListView,
)


urlpatterns = [
    path("", Meal_PlansListView.as_view(), name="meal_plans_list"),
    path("<int:pk>/", Meal_PlansDetailView.as_view(), name="meal_plan_detail"),
    path(
        "<int:pk>/delete/",
        Meal_PlansDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path("new/", Meal_PlansCreateView.as_view(), name="meal_plan_new"),
    path(
        "<int:pk>/edit/", Meal_PlansUpdateView.as_view(), name="meal_plan_edit"
    ),
]
