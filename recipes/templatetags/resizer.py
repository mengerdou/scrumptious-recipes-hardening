from django import template

register = template.Library()


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    ingredients = ingredient.amount
    if servings is not None and target is not None:
        try:
            value = int(target)
            ratio = value / servings
            resized = ingredients * ratio
            return round(resized, 2)
        except ValueError:
            pass
    return ingredients


register.filter(resize_to)
