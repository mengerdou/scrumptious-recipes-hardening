from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    deleteall,
    shoppinnglistcreate,
    RecipeDetailView,
    RecipeListView,
    ShoppingListListView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path(
        "shopping_items/create/",
        shoppinnglistcreate,
        name="add_item",
    ),
    path("shopping_items/", ShoppingListListView.as_view(), name="list_item"),
    path(
        "shopping_items/delete/",
        deleteall,
        name="delete_all",
    ),
]
